# Latest release: [Gugal 0.8.3](https://gitlab.com/gugal/releases/-/blob/main/Gugal-0.8.3-full-release.apk)

## Latest [simple build](https://gugal.gitlab.io/simple.html) _(deprecated)_: [Gugal 0.7 simple](https://gitlab.com/gugal/releases/-/blob/main/simple/Gugal-0.7-simple.apk)

Source code can be found [here](https://gitlab.com/narektor/gugal).