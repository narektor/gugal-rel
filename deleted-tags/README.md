
# Deleted tags
0.3 and 0.2 were removed to prevent F-Droid from releasing them as updates to newer Gugal versions. The APKs and source code are mirrored here.

## 0.3
_last commit: https://gitlab.com/narektor/gugal/-/commit/2e2b194a1c6c84d5b8265098bd608f2e0eb25162 | [source code](0.3/gugal-0.3.zip) | [APK](0.3/Gugal-0.3.apk)_
- API key and CSE ID are now saved.
- Added base for non-Google SERP support (not implemented in the app yet).
- Added dynamic colored icons on Android 12. _Note: might not work correctly in some launchers_
- Improved layout.

## 0.2
_last commit: https://gitlab.com/narektor/gugal/-/commit/04d78e2dbb4022136aaa0ffee24e8789013de14d | [source code](0.2/gugal-0.2.zip) | [APK](0.2/Gugal_0.2.apk)_
- Added search widget
- Tweaked config card
